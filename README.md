# DragonNet [(home)](https://gitlab.inria.fr/GardiNet/dragonnet/)

This is one of the (re)implementations of the protocol DragonNet,
a network coding broadcast protocol (follow-up of DRAGONCAST).

DragonNet implementation was split in several parts:
* dragonnet (this module),
* liblc, a minimal linear coding library, with gaussian elimination decoding [(here)](https://gitlab.inria.fr/GardiNet/liblc/).
* and several other parts (such as NS3 simulation, embedded version on MSP430, ...)

# References

The most related detailed reference is:
- Ichrak Amdouni, Antonia Masucci, Hana Baccouch, Cedric Adjih, "DragonNet: Specification, Implementation, Experimentation and Performance Evaluation", Research Report, Inria, September 2014
([link](https://hal.inria.fr/hal-01632790v1))

# LICENCE

This code is made available for research purposes.

# History

- Another prior C++ library was written by Song Yean Cho and C. Adjih starting from 2006, developped along the protocol DRAGONCAST (implemented in NS2 simulator). The same C++ library was also later used by Farhan Mirani, Anthony Busson, C.A. for the "DONC" opportunistic NC protocol for VANETs.
- As part of the [GETRF project](http://getrf.gforge.inria.fr/category/documentation.html), an evolution of DRAGONCAST for wireless sensor networks was initiated in 2013 as DragonNet/GardiNet with Ichrak Amdouni, Hana Baccouch, Antonia Masucci, C. Adjih - some of results of which are this protocol `dragonnet` and the coding library `liblc`.
