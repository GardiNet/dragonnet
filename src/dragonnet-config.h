/*---------------------------------------------------------------------------
 * DragonNet protocol
 *---------------------------------------------------------------------------
 * Author: Cedric Adjih
 * Copyright 2013-2014 Inria
 * All rights reserved. Distributed only with permission.
 *---------------------------------------------------------------------------*/

#ifndef __DRAGONNET_CONFIG_H__
#define __DRAGONNET_CONFIG_H__

/*---------------------------------------------------------------------------*/

//#define WITH_RANK_ACK

//#define WITH_SOURCE_SMALL_WINDOW
//#define SOURCE_WINDOW 1

//#define WITH_DOUBLE_WINDOW

#define CONF_WITH_FPRINTF
#define WITH_NO_CRC
#undef  WITH_PACKET_LIST
#undef  WITH_RECODE

#undef  OLD_CODE

/*---------------------------------------------------------------------------*/

#endif /* __DRAGONNET_CONFIG_H__ */
