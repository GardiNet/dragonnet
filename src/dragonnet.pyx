#---------------------------------------------------------------------------
# Cython Wrapper for the DragonNet protocol
#---------------------------------------------------------------------------

cimport libc.stdio as stdio
from libc.stdlib cimport malloc, free
from cpython cimport array
import warnings
import array

cimport cswif
from cdragonnet cimport *

#---------------------------------------------------------------------------

import sys
swif_dir = "../../swif-codec/src" # XXX: configurable directory
if swif_dir not in sys.path:
    sys.path.append(swif_dir)
import swif

#---------------------------------------------------------------------------

COEF_POS_NONE = _COEF_POS_NONE
PACKET_ID_NONE = _PACKET_ID_NONE

#---------------------------------------------------------------------------

cdef class DragonnetBase:
    cdef broadcast_t  protocol
    cdef flow_param_t flow_param
    cdef broadcast_config_t config

    def __cinit__(self, node_id, flow_info=None):
        print("cinit", node_id, flow_info, self.__class__)
        self.init(node_id, flow_info)

    cpdef init(self, node_id, flow_info):
        cdef flow_param_t* flow_param_init = NULL
        if flow_info is not None:
            self.flow_param.pkt_interval = flow_info.get("pkt-interval")
            self.flow_param.window_size = flow_info.get("window-size")
            self.flow_param.nb_packet = flow_info.get("nb-symbol")
            flow_param_init = &self.flow_param
        dg_init(&self.protocol, node_id, 0, flow_param_init,
                &self.config, NULL, <void*> self)

    cpdef update_time(self, current_time):
        dg_update_time(&self.protocol, current_time)

    cpdef get_next_wakeup_time(self, current_time):
        next_time = dg_get_next_wakeup_time(&self.protocol, current_time)
        return next_time

    cpdef dump(self):
        dg_pywrite(stdio.stdout, &self.protocol)
        stdio.fprintf(stdio.stdout, "\n")
        stdio.fflush(stdio.stdout)

    cdef format_dg_packet(self, current_time, dg_symbol_t* symbol):
        MAX_PACKET_SIZE = 4096 # XXX
        cdef uint8_t* packet_data = <uint8_t*> malloc(MAX_PACKET_SIZE)
        if packet_data is NULL:
            return None
        #cdef cswif.swif_full_symbol_t *symbol \
        #       = cswif.full_symbol_create_from_source(1, "D", 1)
        cswif.full_symbol_dump(symbol, stdio.stdout)
        print("that was generated symbol")
        packet_size = dg_generate_coded_packet(
            &self.protocol, packet_data, MAX_PACKET_SIZE,
            current_time, symbol)
        cswif.full_symbol_free(symbol)
        if packet_size == 0:
            return None
        try:
            result = packet_data[:packet_size]
        finally:
            free(packet_data)
        # XXX:check https://stackoverflow.com/questions/18462785/what-is-the-recommended-way-of-allocating-memory-for-a-typed-memory-view
        return result

    cpdef receive_dg_packet(self, current_time, packet):
        packet_size = len(packet)
        cdef uint8_t* raw_packet = packet # XXX: unsafe (?)
        return dg_process_coded_packet(
            &self.protocol, raw_packet, packet_size, current_time)

    cpdef generate_dg_packet(self, current_time, min_symbol_id, max_symbol_id):
        cdef dg_symbol_t* symbol = dg_create_coded_packet(
            &self.protocol, min_symbol_id, max_symbol_id)
        if symbol is NULL:
            return None

        self.format_dg_packet(current_time, symbol)
        free(symbol)

    #--------------------------------------------------
    # High-level functions (template methods)
    #--------------------------------------------------

    # dg_intermediate_create_coded_packet(broadcast, min_coef_pos, max_coef_pos,
    #                                     coded_packet)

    # dg_create_coded_packet(broadcast, min_coef_pos, max_coef_pos,
    #                        coded_packet)
    
    #def generate_symbol(self, XXX, XXX):
    #    pass

    #--------------------------------------------------
    # Codec API part (will be later moved and implemented
    # from SWIF)
    #--------------------------------------------------

    cpdef codec_get_rank(self):
        warnings.warn("codec_get_rank not implemented")
        return 0

    cpdef codec_get_low_index(self):
        warnings.warn("codec_low_index not implemented")
        return 0

    cpdef codec_get_high_index(self):
        warnings.warn("codec_high_index not implemented")
        return 0

    cdef dg_symbol_t* codec_create_source_symbol(self, first_id, last_id):
        warnings.warn("codec_create_source_symbol not implemented")        
        py_result = swif.FullSymbol((first_id, last_id-first_id+1, b""))
        py_result.dump()
        #cdef cswif.swif_full_symbol_t* result = cswif.full_symbol_to_c(
        #     <void*>py_result)
        #return result
        #swif.full_symbol_to_c(<void*>py_result)        
        return NULL

#---------------------------------------------------------------------------
# These functions are defined in `dragonnet-codec.h' and the implementation
# below is being used (by `libdragonnet.a')
#---------------------------------------------------------------------------

cdef public unsigned int dg_codec_get_rank(dg_codec_t codec):
    protocol = <object> codec
    return protocol.codec_get_rank()

cdef public unsigned int dg_codec_get_low_index(dg_codec_t codec):
    protocol = <object> codec
    return protocol.codec_get_low_index()

cdef public unsigned int dg_codec_get_high_index(dg_codec_t codec):
    protocol = <object> codec
    return protocol.codec_get_high_index()

cdef public unsigned int dg_codec_add_symbol(
    dg_codec_t codec, dg_symbol_t* symbol):
    protocol = <object> codec
    cdef unsigned int u = <unsigned int> symbol;
    print("symbol: %08x"% u)

cdef public unsigned int dg_codec_pack_symbol(
    dg_codec_t codec, dg_symbol_t* symbol, uint8_t **result_data):
    protocol = <object> codec
    cdef unsigned int i = <unsigned int> symbol
    print("pack symbol", i)
    
cdef public dg_symbol_t* dg_codec_unpack_symbol(
    dg_codec_t codec, uint8_t* data, unsigned int size):
    protocol = <object> codec
    pass

cdef public dg_symbol_t* dg_codec_create_source_symbol(
    dg_codec_t codec, uint16_t first_symbol_id, uint16_t last_symbol_id):
    cdef DragonnetBase protocol = <object> codec
    return protocol.codec_create_source_symbol(first_symbol_id, last_symbol_id)

#---------------------------------------------------------------------------

# //~ uint16_t  packet_size = dg_generate_coded_packet (next_broadcast_t,
# //~ packet_data, max_packet_size, current_time, &next_coded_packet);
# //~ broadcast_t* receiver = node_table[link];
# //~ dg_process_coded_packet(receiver, packet_data, packet_size, current_ti

#---------------------------------------------------------------------------
