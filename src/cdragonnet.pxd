#---------------------------------------------------------------------------
# Cython Wrapper for the DragonNet protocol
#---------------------------------------------------------------------------

from libc.stdint cimport uint8_t, uint16_t, uint32_t, int64_t, bool
from libc.stdio cimport FILE

cimport cswif


ctypedef cswif.swif_full_symbol_t dg_symbol_t

#---------------------------------------------------------------------------


#---------------------------------------------------------------------------

cdef extern from "dragonnet-protocol.h":
    # constant export: https://stackoverflow.com/questions/31766219/export-constants-from-header-with-cython
    cdef enum: _COEF_POS_NONE "COEF_POS_NONE"
    cdef enum: _PACKET_ID_NONE "PACKET_ID_NONE"

    ctypedef int bool_t # XXX 
    ctypedef double broadcast_time_t #XXX   
    ctypedef double interval_t # XXX

#---------------------------------------------------------------------------

cdef extern from "dragon.h":
    cdef enum: _DRAGON_MAX_NEIGHBOR "DRAGON_MAX_NEIGHBOR"

    ctypedef struct dragon_info_t:
        uint16_t rank;
        uint16_t high_index;
        uint16_t low_index;
        uint16_t nb_neighbor;
        broadcast_time_t last_time;

    ctypedef struct dragon_neighbor_t:
        uint16_t identifier;
        uint16_t priority;
        dragon_info_t info;
        uint16_t count_recv;

    ctypedef struct dragon_t:
        dragon_neighbor_t neighbor[_DRAGON_MAX_NEIGHBOR]

#---------------------------------------------------------------------------

cdef extern from "dragonnet-protocol.h":

    ctypedef struct broadcast_callback_t:
        pass

    #ctypedef struct dg_codec_t:
    #    pass

    ctypedef void* dg_codec_t

    #ctypedef struct dg_symbol_t:
    #    pass

    ctypedef struct flow_param_t:
        interval_t pkt_interval;
        uint16_t window_size;
        uint16_t nb_packet;

    ctypedef struct broadcast_config_t:
        uint16_t conf_expire_interval

    ctypedef struct broadcast_t:
        broadcast_time_t current_time;
        uint16_t         node_identifier;

        broadcast_time_t pkt_interval;
        broadcast_time_t last_transmit_time;
        uint16_t         last_seq_num;

        bool_t           is_nc_source;
        flow_param_t     flow_param; 
        broadcast_config_t broadcast_config;
        dragon_t         dragon;

        broadcast_callback_t* callback;

    void dg_init(
        broadcast_t* self, uint16_t identifier, 
        broadcast_time_t current_time,
        flow_param_t* flow_param,
        broadcast_config_t* broadcast_config,
        broadcast_callback_t* callback,
	dg_codec_t codec);

    uint16_t dg_process_coded_packet(
        broadcast_t* self, uint8_t* packet_data, 
        uint16_t packet_size, broadcast_time_t current_time);

    uint16_t dg_generate_coded_packet(
        broadcast_t* self, uint8_t* packet_data, uint16_t max_packet_size, 
        broadcast_time_t current_time, dg_symbol_t* coded_packet);

    dg_symbol_t* dg_create_coded_packet(
        broadcast_t* self, uint16_t min_coef_pos, uint16_t max_coef_pos);

    broadcast_time_t dg_get_next_wakeup_time(
        broadcast_t* self, broadcast_time_t current_time);

    uint16_t dg_add_coded_packet(broadcast_t* self, dg_symbol_t* pkt);

    void dg_update_time(broadcast_t* self, broadcast_time_t current_time);

    uint16_t dg_generate_test_source_packet(
        void* arg, uint16_t packet_id, uint8_t* packet_data,
        uint16_t max_packet_size);

    uint16_t dg_get_rank(broadcast_t* self);

    void dg_update_rate(broadcast_t* self);

    void dg_pywrite(FILE* out, broadcast_t* state);

#---------------------------------------------------------------------------
