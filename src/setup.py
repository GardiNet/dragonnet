#---------------------------------------------------------------------------

from distutils.core import setup
from distutils.extension import Extension
from Cython.Build import cythonize
import os
import sys

#---------------------------------------------------------------------------

swif_dir = "../../swif-codec/src" # XXX: configurable directory

extensions = [
    Extension(
        "dragonnet", ["dragonnet.pyx"],
        include_dirs=[swif_dir], 
        libraries=["swif", "dragonnet"], # XXX: swif redundant?
        library_dirs=[swif_dir, "."]
    ),
]

module = cythonize("dragonnet.pyx", language_level=3)

setup(name='DragonNet', ext_modules=cythonize(extensions))

#---------------------------------------------------------------------------
