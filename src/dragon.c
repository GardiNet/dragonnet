/*---------------------------------------------------------------------------
 * DragonNet protocol
 *---------------------------------------------------------------------------
 * Author: Cedric Adjih, Hana Baccouch
 * Copyright 2013-2014 Inria
 * All rights reserved. Distributed only with permission.
 *---------------------------------------------------------------------------*/

#include <stdbool.h>

#include "general.h"
#include "buffer.h"
#include "dragon.h"
#include "dragonnet-protocol.h"
/*---------------------------------------------------------------------------*/
//#ifndef EMBEDDED
uint16_t dg_expire_time =1 ;
//#else
//extern uint16_t dg_expire_time;
//#endif
static dragon_neighbor_t* dragon_find_neighbor
(dragon_t* self, uint16_t neighbor_id, bool_t should_create)
{
  int i;
  //uint16_t free_priority = PRIORITY_NONE;
  int free_index = -1;
  for (i=0; i<DRAGON_MAX_NEIGHBOR; i++) {
    dragon_neighbor_t* neighbor = &self->neighbor[i];
    if (neighbor_id == neighbor->identifier)
      return neighbor;
    else if (neighbor->identifier == NODE_ID_NONE) {
      //&& neighbor->priority < free_priority) {
      if (free_index == -1)
	free_index = i;
      //free_priority = neighbor->priority;
    }
  }

  if (free_index < 0 || !should_create)
    return NULL;
  return &self->neighbor[free_index];
}

void dragon_init(dragon_t* self, struct s_broadcast_config_t* broadcast_config)
{
  memset(self->neighbor, 0, sizeof(self->neighbor));
  uint16_t i;
  for (i=0; i<DRAGON_MAX_NEIGHBOR; i++) {
    dragon_neighbor_t* neighbor = &self->neighbor[i];
    neighbor->identifier = NODE_ID_NONE;
    neighbor->count_recv = 0;
  }
  dg_expire_time = broadcast_config->conf_expire_interval * DG_TIME_UNIT ;
}

uint16_t dragon_count_neighbor(dragon_t* self)
{
  uint16_t result = 0;
  uint16_t i;
  for (i=0; i<DRAGON_MAX_NEIGHBOR; i++) {
    dragon_neighbor_t* neighbor = &self->neighbor[i];
    if (neighbor->identifier != NODE_ID_NONE)
      result++;
  }
  return result;
}

void dragon_process(dragon_t* self, uint16_t neighbor_id,
		    dragon_info_t* neighbor_info)
{
  dragon_neighbor_t* neighbor = dragon_find_neighbor
    (self, neighbor_id, true);

  if (neighbor == NULL) {
    WARN("Neighbor table is full");
    return ;
  }

  if (neighbor->identifier == NODE_ID_NONE) {
    neighbor->identifier = neighbor_id;
    neighbor->priority = 0;
    neighbor->count_recv = 0;
  }
  memcpy(&neighbor->info, neighbor_info, sizeof(dragon_info_t));
  neighbor->count_recv++;
}

uint16_t dragon_get_lowest_coef(dragon_t* self, broadcast_time_t current_time)
{
  uint16_t result = COEF_POS_NONE;
  uint16_t i;
  for (i=0; i<DRAGON_MAX_NEIGHBOR; i++) {
    dragon_neighbor_t* neighbor = &self->neighbor[i];
    if (neighbor->identifier != NODE_ID_NONE 
	&& neighbor->info.last_time+dg_expire_time >= current_time) {
#ifdef WITH_RANK_ACK
      if (result == COEF_POS_NONE || neighbor->info.rank < result)
	result = neighbor->info.rank;
#else /* WITH_RANK_ACK */
      if (result == COEF_POS_NONE || neighbor->info.low_index < result)
	result = neighbor->info.low_index;
#endif /* WITH_RANK_ACK */
    }
  }  
  return result;
}

uint16_t dragon_get_lowest_rank(dragon_t* self, broadcast_time_t current_time)
{
  uint16_t result = COEF_POS_NONE;
  uint16_t i;
  for (i=0; i<DRAGON_MAX_NEIGHBOR; i++) {
    dragon_neighbor_t* neighbor = &self->neighbor[i];
    if (neighbor->identifier != NODE_ID_NONE 
	&& neighbor->info.last_time+dg_expire_time >= current_time) {
      if (result == COEF_POS_NONE || neighbor->info.rank < result)
	result = neighbor->info.rank;
    }
  }  
  return result;
}

double dragon_compute_packet_rate(dragon_t* self, uint16_t dg_rank)
{
  uint16_t i;
  double rate = 0;
  //~ uint16_t self_nb_neighbor = dragon_count_neighbor(self);
  for (i=0; i<DRAGON_MAX_NEIGHBOR; i++) {
    dragon_neighbor_t* neighbor = &self->neighbor[i];
    if (neighbor->identifier != NODE_ID_NONE ) {
      double result = 0;
      uint16_t rank = neighbor->info.rank;
        if (neighbor->info.nb_neighbor!=0) {
          result = (dg_rank-rank) / (double)neighbor->info.nb_neighbor;
          fprintf(stdout, "Neigh, Packet-Rate:%lf, dg-rank:%d, neigh-rank:%d,  nb-neigh:%d", rate, dg_rank, rank, neighbor->info.nb_neighbor);
        }
        else {
          result = (dg_rank-rank) / 1;
          fprintf(stdout, "No Neigh, Packet-Rate:%lf, dg-rank:%d, neigh-rank:%d,  nb-neigh:%d", rate, dg_rank, rank, neighbor->info.nb_neighbor);
        }
        if (result>rate && result!=65535)
          rate = result;
    }
  }
  fprintf(stdout, ", Packet Rate:%lf", rate);
  return rate*RATE_ADAPTATION_CONSTANT/(double)10.0;
}
/*---------------------------------------------------------------------------*/

#ifdef CONF_WITH_FPRINTF

void dragon_pywrite(FILE* out, dragon_t* self, broadcast_time_t current_time)
{
  int i;
  //broadcast_time_t expired_time = dg->current_time + dg_expire_time;
  fprintf(out, "{ 'type':'dragon'");
  fprintf(out, ", 'neighborTable': {");
  bool_t is_first = true;
  for (i=0; i< DRAGON_MAX_NEIGHBOR; i++) {
    dragon_neighbor_t* neighbor = &self->neighbor[i];
    
    if (neighbor->identifier == NODE_ID_NONE)
      continue;
    if (is_first) { is_first = false; }
    else fprintf(out,","); 
      
    fprintf(out, "%d: { 'nodeId':%d", i, neighbor->identifier);
    fprintf(out, ", 'priority': %d, 'rank': %d, 'highIndex':%d, 'lowIndex':%d"
	    ", 'nbNeighbor':%d, 'count':%d, 'lastTime':%" PRI_DG_TIME
	    ", 'active':%d",
	    neighbor->priority, neighbor->info.rank, 
	    neighbor->info.high_index, neighbor->info.low_index, 
	    neighbor->info.nb_neighbor, neighbor->count_recv,
	    neighbor->info.last_time,
	    (neighbor->info.last_time+dg_expire_time >= current_time) );
    fprintf(out, "}");
  }
  fprintf(out, " } }");
}

#endif /* WITH_FPRINTF */

/*---------------------------------------------------------------------------*/
