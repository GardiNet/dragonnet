#---------------------------------------------------------------------------
# Simple test for the DragonNet protocol
#---------------------------------------------------------------------------

import swif
import dragonnet

#---------------------------------------------------------------------------

class Dragonnet(dragonnet.DragonnetBase):

    def __init__(self, node_id, flow_info):
        if flow_info == None:
            # print intermediate node
            self.is_source = False
        else:
            self.is_source = True

    def get_packet(self):
        pass

#---------------------------------------------------------------------------

flow_info = {
    "pkt-interval": 1,
    "window-size": 5,
    "nb-symbol": 100
}

source = Dragonnet(0, flow_info)
source.dump()

source.update_time(0)
#print(source.get_next_wakeup_time(0))
packet = source.generate_dg_packet(0, 0, 10)
print("packet:", packet)

receiver = Dragonnet(0, flow_info=None)
receiver.dump()
receiver.update_time(0)
print(receiver.receive_dg_packet(0, packet))

receiver.dump()

#---------------------------------------------------------------------------
