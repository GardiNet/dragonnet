/* General definitions */

#ifndef __GENERAL_H__
#define __GENERAL_H__

#include <assert.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#define BEGIN_MACRO do {
#define END_MACRO } while(0)

static const bool BOOL_TRUE = true;
static const bool BOOL_FALSE = false;
typedef bool bool_t;
typedef double broadcast_time_t;
#define PRI_DG_TIME "lf"

#define WARN(...) BEGIN_MACRO fprintf( stderr, __VA_ARGS__ ); END_MACRO
#define ASSERT(...) assert(__VA_ARGS__)
#define FATAL(...) BEGIN_MACRO WARN(__VA_ARGS__); exit(1); END_MACRO
#define UNUSED(var_name) BEGIN_MACRO (void)(var_name); END_MACRO

#define LOGLOG2_FIELD_SIZE

#endif /* __GENERAL_H__ */

