/*---------------------------------------------------------------------------
 * Interface between DragonNet and the codec
 *---------------------------------------------------------------------------
 * For now, the interface is implemented in the cython file `dragonnet.pyx',
 * which in turn uses the SWIF codec, to test different implementations.
 * Ultimately the SWIF API could be called directly (with proper extensions).
 *---------------------------------------------------------------------------*/

#ifndef __DRAGONNET_CODEC_H__
#define __DRAGONNET_CODEC_H__

#ifdef __cplusplus
extern "C" {
#endif

/*---------------------------------------------------------------------------*/

typedef void* dg_codec_t;
typedef swif_full_symbol_t dg_symbol_t;

unsigned int dg_codec_get_rank(dg_codec_t codec);
unsigned int dg_codec_get_low_index(dg_codec_t codec);
unsigned int dg_codec_get_high_index(dg_codec_t codec);  
unsigned int dg_codec_add_symbol(dg_codec_t codec, dg_symbol_t* symbol);
unsigned int dg_codec_pack_symbol(dg_codec_t codec, dg_symbol_t* symbol,
         		          uint8_t **result_data);
dg_symbol_t* dg_codec_unpack_symbol(dg_codec_t codec,
         		            uint8_t* data, unsigned int size);
dg_symbol_t* dg_codec_create_source_symbol
(dg_codec_t codec, uint16_t min_coef_pos, uint16_t max_coef_pos);

/*---------------------------------------------------------------------------*/

#ifdef __cplusplus
}
#endif

#endif /* __DRAGONNET_CODEC_H__ */

