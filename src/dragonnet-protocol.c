/*---------------------------------------------------------------------------
 * DRAGONNET protocol
 *---------------------------------------------------------------------------
 * Author: Cedric Adjih, Hana Baccouch
 * Copyright 2013-2014 Inria
 * All rights reserved. Distributed only with permission.
 *---------------------------------------------------------------------------*/

#include <stdbool.h>
#include "buffer.h"
#include "dragonnet-protocol.h"
#include "dragon.c"

#define L 1

/*---------------------------------------------------------------------------*/

#ifndef OLD_CODE
#define WARN_NOT_REIMPLEMENTED WARN(\
	     "WARNING in function'%s': not reimplemented yet.\n", __func__)
#endif /* OLD_CODE */

/*---------------------------------------------------------------------------*/

#ifdef WITH_PACKET_LIST
void packet_list_init(packet_list_t *list);
#endif /* WITH_PACKET_LIST */

static void dg_update_pkt_interval(broadcast_t* self);

/*---------------------------------------------------------------------------*/

/* Sample functions */

void notify_packet_decoded(dg_codec_t* codec, uint16_t packet_index)
{
#if 0  
  packet_set->nb_decoded_packet++; // XXX

  if (packet_set->notif_data != NULL) {
    broadcast_t* self = (broadcast_t*) packet_set->notif_data;
    if (self->callback != NULL 
	&& self->callback->notify_packet_decoded_function != NULL) {
      self->callback->notify_packet_decoded_function
	(self->callback->callback_arg, packet_index);
      return;
    }
  }
#endif
  printf("*** decoded pkt#%u: ", packet_index);
  printf("\n");
}

void notify_set_full
(dg_codec_t* codec, uint16_t required_min_coef_pos)
{
#if 0  
  //packet_set_count(packet_set);
  for (;;) {
    uint8_t ok = packet_set_free_first(packet_set);
    if (!ok)
      return;
    ASSERT( ok );
    
    if (packet_set_is_empty(packet_set))
      return; /* emptied packet set, there should be room now */

   if (packet_set->coef_pos_min >= required_min_coef_pos)
      return; /* sufficient number of packets removed */
  }
#endif  
}

static bool_t get_decoded_packet
(dg_codec_t* codec, uint16_t required_min_coef_pos,
 dg_symbol_t* res_coded_packet);

/*---------------------------------------------------------------------------*/

static void buffer_put_flow_param(buffer_t* buffer, flow_param_t* param)
{
  buffer_put_u16(buffer, param->pkt_interval);
  buffer_put_u16(buffer, param->window_size);
  buffer_put_u16(buffer, param->nb_packet);
}

static void buffer_get_flow_param(buffer_t* buffer, flow_param_t* param)
{
  param->pkt_interval = buffer_get_u16(buffer);
  param->window_size = buffer_get_u16(buffer);
  param->nb_packet = buffer_get_u16(buffer);
}
/*---------------------------------------------------------------------------*/

void dg_init(broadcast_t* self, uint16_t identifier, 
	     broadcast_time_t current_time,
	     flow_param_t* flow_param,
	     broadcast_config_t* broadcast_config,
	     broadcast_callback_t* callback,
	     dg_codec_t* codec)
{ 
  self->node_identifier = identifier;
  self->current_time = current_time;
  self->last_transmit_time = current_time;
  self->pkt_interval = MAX_PKT_INTERVAL;
  self->last_seq_num = 0;
  self->is_nc_source = (flow_param != NULL);
  if (flow_param != NULL) {
    //ASSERT( flow_param->window_size <= log2_window_size(L) );
    self->flow_param = *flow_param;
  } else {
    self->flow_param.window_size = 0;
    self->flow_param.nb_packet = 0;
    self->flow_param.pkt_interval = 0;
  }
  self->broadcast_config.conf_expire_interval=1;
  self->callback = callback;
  self->codec = codec;

#ifdef OLD_CODE  
  notify_packet_decoded_func_t notify_decoded_func = notify_packet_decoded;
  notify_set_full_func_t notify_full_func = notify_set_full;
  
  packet_set_init(& self->packet_set, L, notify_decoded_func,
		  notify_set_full,  get_decoded_packet, self);
#else
  WARN("Need to initialize codec.\n");
#endif
  
#ifdef WITH_PACKET_LIST
  packet_list_init(&self->packet_list); 
#endif /* WITH_PACKET_LIST */
  dragon_init(&self->dragon, broadcast_config);
}

void dg_set_codec(broadcast_t* self, dg_codec_t* codec)
{
  self->codec = codec;
}

#if 0
void generate_source_packet(packet_set_t* set, packet_list_t* list,
			    uint16_t packet_size)
{
  ASSERT( packet_size <= CODED_PACKET_SIZE );
  uint16_t _L = set->log2_nb_bit_coef;
  uint16_t window_size = log2_window_size(_L);
  if (window_size > MAX_CODED_PACKET)
    window_size = MAX_CODED_PACKET;
  
  uint16_t counter = 0;
  while (counter < window_size) {
    uint16_t i; 
    coded_packet_t coded_pkt;
    uint16_t packet_id;
    coded_packet_init(&coded_pkt, set->log2_nb_bit_coef);
   
    for (i=0; i< window_size; i++) {    
      uint8_t packet[packet_size];
      memset(packet, i, packet_size);
      coded_packet_t pkt;
      memset(&pkt, 0, sizeof(pkt));
      coded_packet_init_from_base_packet(&pkt, _L, i, packet, packet_size);
      uint8_t coef=rand() % (1<<(1<<_L));
      coded_packet_add_mult (&coded_pkt, coef, &pkt);
    }
    packet_id = dg_add_coded_packet(self, &coded_pkt);
    if (packet_id != PACKET_ID_NONE)
      counter++;
  }

}
#endif

/*---------------------------------------------------------------------------*/

#ifndef WITH_NO_CRC
#include "crc16.c"
#endif /* WITH_NO_CRC */

uint16_t dg_generate_test_source_packet
(void* arg, uint16_t packet_id, uint8_t* packet_data,
 uint16_t max_packet_size)
{
  memset(packet_data, packet_id & 0xffu, max_packet_size);
#ifndef WITH_NO_CRC
  if (max_packet_size > 2) {
    uint16_t checksum = crc16_web(packet_data+2, max_packet_size-2);
    DIRECT_PUT_U16(packet_data, checksum);
    //printf("checksum=%04x size=%d\n", checksum, max_packet_size-2);
  }
#endif
  return max_packet_size;
}

#ifdef EMBEDDED
#define MY_RAND (RAND_CLOCK^(uint16_t)rand())
#else /* EMBEDDED */
#define MY_RAND (rand())
#endif /* EMBEDDED */

/* see: generate_source_packet */
/** generate a linear combination of source packets */
dg_symbol_t* dg_source_create_coded_packet
(broadcast_t* self, uint16_t min_coef_pos, uint16_t max_coef_pos)
{

#if OLD_CODE
  ASSERT( min_coef_pos <= max_coef_pos );
  //ASSERT( max_packet_size <= CODED_PACKET_SIZE );
  ///printf("@ SOURCE GEN: %d %d\n", min_coef_pos, max_coef_pos);

  uint16_t _L = self->packet_set.log2_nb_bit_coef;
  uint16_t window_size = 1<<log2_window_size(_L);

  ASSERT( max_coef_pos - min_coef_pos +1 <= window_size );
  ASSERT( max_coef_pos - min_coef_pos +1 <= MAX_CODED_PACKET );

  uint16_t i; 

#ifdef WITH_SOURCE_SMALL_WINDOW
  uint16_t coef_pos = min_coef_pos;
  if (max_coef_pos > min_coef_pos)
    coef_pos += MY_RAND % (max_coef_pos-min_coef_pos+1);
  min_coef_pos = coef_pos;
  if (coef_pos+SOURCE_WINDOW-1 < max_coef_pos)
    max_coef_pos = coef_pos+SOURCE_WINDOW-1;
#endif /* WITH_SOURCE_SMALL_WINDOW */

  coded_packet_init(res_coded_packet, _L);
  for (i=min_coef_pos; i<=max_coef_pos; i++) {
    coded_packet_t source_coded_packet;
    uint8_t source_packet_content[CODED_PACKET_SIZE];
    uint16_t actual_size = 0;

    if (self->callback == NULL 
	|| self->callback->generate_source_packet_function == NULL) {
      ///printf("@g1 ");
      actual_size = dg_generate_test_source_packet
	(self, i, source_packet_content, 6); /* XXX: packet size */
    } else {
      ///printf("@g2 ");
      actual_size = self->callback->generate_source_packet_function
	(self, i, source_packet_content, CODED_PACKET_SIZE);
    }

    ///printf("@g3 %d\n", actual_size);
    if (actual_size == 0) 
      continue;

    coded_packet_init_from_base_packet(&source_coded_packet, _L, i,
				       source_packet_content, actual_size);

#ifdef EMBEDDED
    uint8_t coef=(RAND_CLOCK^(uint8_t)rand()) % (1<<(1<<L));
#else
    uint8_t coef=rand() % (1<<(1<<L));
#endif      
    if (min_coef_pos == max_coef_pos)
      coef = 1; /* no need for a linear combination */
    ///printf("@g4 coef=%u\n", coef);
    coded_packet_add_mult (res_coded_packet, coef, &source_coded_packet);
  }
#else /* OLD_CODE */
  return dg_codec_create_source_symbol(self->codec, min_coef_pos, max_coef_pos);
#endif /* OLD_CODE */
  return NULL;
}

/*---------------------------------------------------------------------------*/

dg_symbol_t* dg_intermediate_create_coded_packet
(broadcast_t* self, uint16_t min_coef_pos, uint16_t max_coef_pos)
{
#ifdef OLD_CODE
  coded_packet_init(coded_packet, self->packet_set.log2_nb_bit_coef);

#ifdef WITH_PACKET_LIST
  packet_list_t* packet_list = &self->packet_list;
#else  
  packet_list_t* packet_list = NULL;
#endif /* WITH_PACKET_LIST */

#ifdef WITH_RECODE
  packet_set_generate_coded_packet (&self->packet_set, packet_list,
				    min_coef_pos, max_coef_pos, coded_packet);
#else
  FATAL("not implemented yet");
#endif /* WITH_RECODE */
  return !coded_packet_was_empty(coded_packet);

#else /* OLD_CODE */
  WARN_NOT_REIMPLEMENTED;  
  return NULL;
#endif /* OLD_CODE */  
}

dg_symbol_t* dg_create_coded_packet
(broadcast_t* self, uint16_t min_coef_pos, uint16_t max_coef_pos)
{
  if (self->is_nc_source) {
    dg_symbol_t* result = dg_source_create_coded_packet
      (self, min_coef_pos, max_coef_pos);
    if (result != NULL) {
      dg_add_coded_packet(self, result);
    }
    return result;
  } else {
    dg_symbol_t* result = dg_intermediate_create_coded_packet 
      (self, min_coef_pos, max_coef_pos);
    return result;    
  }
}

/*---------------------------------------------------------------------------*/

static bool_t dg_has_flow_param(broadcast_t* self)
{ return self->flow_param.nb_packet != 0; }

uint16_t dg_process_coded_packet
(broadcast_t* self, uint8_t* packet_data,
 uint16_t packet_size, broadcast_time_t current_time)
{
  self->current_time = current_time;

#ifdef OLD_CODE
  
#ifdef EMBEDDED
  uint16_t preTAR = TAR;

  dg_symbol_t tmp_coded_packet;
  dg_symbol_t* coded_packet = &tmp_coded_packet;
#else /* CONTIKI */
  dg_symbol_t* coded_packet = (dg_symbol_t*) 
    malloc(sizeof(*coded_packet));
#endif

#else /* OLD_CODE */
  dg_symbol_t* coded_packet = NULL;
#endif /* OLD_CODE */
  
  buffer_t buffer;
  buffer_init(&buffer, packet_data, packet_size);
  
  /*** NodeId / SeqNumber/ CoefPosMin/ CoefPosMax/ dataSize/ data ***/
  uint16_t sender_node_id = buffer_get_u16(&buffer);
  uint16_t sender_seq_num = buffer_get_u16(&buffer);
  UNUSED(sender_seq_num);
  
  /* dragon params */
  dragon_info_t dragon_info;
  dragon_info.rank = buffer_get_u16(&buffer);
  dragon_info.low_index = buffer_get_u16(&buffer);
  dragon_info.high_index = buffer_get_u16(&buffer);
  dragon_info.nb_neighbor = buffer_get_u16(&buffer);
  dragon_info.last_time = current_time;

  /* flow params */
  flow_param_t flow_param;
  buffer_get_flow_param(&buffer, &flow_param);

  /* packet params */
#ifdef OLD_CODE
  coded_packet->log2_nb_bit_coef = L;  
  coded_packet->coef_pos_min = buffer_get_u16(&buffer);
  coded_packet->coef_pos_max = buffer_get_u16(&buffer);
  coded_packet->data_size =  buffer_get_u16(&buffer);
#else  /* OLD_CODE */
  WARN_NOT_REIMPLEMENTED;  
#endif /* OLD_CODE */
  //uint16_t content_size = buffer_get_u16(&buffer);
  uint16_t receiver_node_id = self->node_identifier;
  UNUSED(receiver_node_id);
  //if (content_size != sizeof(coded_packet.content.u8)) {
  /*if (coded_packet.data_size != sizeof(coded_packet.content.u8)) {
    printf("warning: incorrect content size = %u %lu\n", coded_packet.data_size,
      sizeof(coded_packet.content.u8));
    return;
  }
  */
#ifdef OLD_CODE
  coded_packet->log2_nb_bit_coef = L;  
  coded_packet->coef_pos_min = buffer_get_u16(&buffer);
  coded_packet->coef_pos_max = buffer_get_u16(&buffer);
  coded_packet->data_size =  buffer_get_u16(&buffer);

  buffer_get_data(&buffer, coded_packet->content.u8, //coded_packet->data_size);
		  sizeof(coded_packet->content.u8));
#else  /* OLD_CODE */
  WARN_NOT_REIMPLEMENTED;  
#endif /* OLD_CODE */

  uint16_t packet_id = PACKET_ID_NONE;
  if (!self->is_nc_source) { 
    if (!dg_has_flow_param(self)) {
      self->flow_param = flow_param;
    } else {
      /* XXX: TODO : check if same params */
    }
  }
  packet_id = dg_add_coded_packet(self, coded_packet);

  dragon_process(&self->dragon, sender_node_id, &dragon_info
		 /*packet_id != PACKET_ID_NONE*/);
  dg_update_pkt_interval(self);

#ifdef EMBEDDED
  /// printf("RECV(%u,%u,%04x:%u): %u\n", preTAR, TAR, sender_node_id, sender_seq_num, packet_id);
#else /* EMBEDDED */

  /* printf("\nSTATE-INFO: {'time':%lu, 'senderPacketId':,%u," */
  /* 	 "'receiverId': %u,'senderId': %u, 'decodedCount':%u" */
  /* 	 /\*, 'packetIdInSet': %u*\/"}\nLONG-STATE-INFO: ", */
  /*   current_time, sender_seq_num ,receiver_node_id, sender_node_id, */
  /*   (&self->packet_set)->nb_decoded_packet/\*, pkid*\/); */

  /* packet_set_pywrite(stdout, &self->packet_set); */
  free(coded_packet);
#endif /* EMBEDDED */

  return packet_id;
}

/*--------------------------------------------------*/

uint16_t dg_get_rank(broadcast_t* self)
{
  /* XXX: does not take decoding losses into account */
#ifdef OLD_CODE  
  uint16_t rank = self->packet_set.nb_decoded_packet
    + packet_set_count(&self->packet_set, false);
#else  /* OLD_CODE */
  uint16_t rank = 0;  
  if (self->codec != NULL)
    rank = dg_codec_get_rank(self->codec);
#endif /* OLD_CODE */
  return rank;
}

static broadcast_time_t dg_get_pkt_interval(broadcast_t* self)
{
  /* XXX: move to dragon.c */
  uint16_t i;
  int my_rank = dg_get_rank(self);
  uint16_t result = 0;
  bool_t has_result = false;
  for (i=0; i<DRAGON_MAX_NEIGHBOR; i++) {
    dragon_neighbor_t* neighbor = &self->dragon.neighbor[i];
    if (neighbor->identifier != NODE_ID_NONE
	&& neighbor->info.last_time+dg_expire_time >= self->current_time) {
      int neigh_rank = neighbor->info.rank;
      if (neigh_rank < my_rank) {
	uint16_t neigh_gap = my_rank - neigh_rank;
	if (neigh_gap > MAX_NEIGHBOR_GAP)
	  neigh_gap = MAX_NEIGHBOR_GAP;
	uint16_t nb_two_hop = neighbor->info.nb_neighbor;
	if (nb_two_hop > MAX_NEIGHBOR_NB_TWO_HOP)
	  nb_two_hop = MAX_NEIGHBOR_NB_TWO_HOP;
	if (nb_two_hop == 0)
	  nb_two_hop = 1;
	uint16_t ratio = (nb_two_hop << GAP_SHIFT) / neigh_gap;
	if (!has_result || ratio < result)
	  result = ratio;
      }
    }
  }
  ///printf("\n");
  return result;
}

static void dg_update_pkt_interval(broadcast_t* self)
{ 
  broadcast_time_t interval = dg_get_pkt_interval(self); 
  if (interval > MAX_PKT_INTERVAL)
    interval = MAX_PKT_INTERVAL;
  if (interval < MIN_PKT_INTERVAL)
    interval = MIN_PKT_INTERVAL;
  self->pkt_interval = interval;
}

void dg_update_time(broadcast_t* self, broadcast_time_t current_time)
{
  self->current_time = current_time;
  dg_update_pkt_interval(self);
}

#ifndef EMBEDDED
void dg_update_rate(broadcast_t* self)
{
  uint16_t dg_rank = dg_get_rank(self);
  uint16_t rate = dragon_compute_packet_rate(&self->dragon, dg_rank);
  self->flow_param.pkt_interval = 1000/rate ;
}  
#endif /* EMBEDDED */

uint16_t dg_generate_coded_packet
(broadcast_t* self, uint8_t* packet_data, uint16_t max_packet_size, 
  broadcast_time_t current_time, dg_symbol_t* coded_packet)
{  
  self->current_time = current_time;
  
  /** NodeId / SeqNumber/ CoefPosMin/ CoefPosMax/ dataSize/ data **/
  buffer_t buffer;
  buffer_init(&buffer, packet_data, max_packet_size);
  uint16_t node_id = self->node_identifier;
  buffer_put_u16(&buffer, node_id);
  self->last_seq_num++;
  buffer_put_u16(&buffer, self->last_seq_num);

  /* dragon params */
#ifdef OLD_CODE  
  uint16_t low_index = packet_set_get_low_index(&self->packet_set);
  uint16_t high_index = self->packet_set.coef_pos_max;
#else /* OLD_CODE */

  uint16_t low_index = 0;
  uint16_t high_index = 0;
  if (self->codec != NULL) {
    low_index = dg_codec_get_low_index(self->codec);
    high_index = dg_codec_get_high_index(self->codec);
  }
#endif /* OLD_CODE */

  buffer_put_u16(&buffer, dg_get_rank(self));
  buffer_put_u16(&buffer, low_index); /* XXX: what if NONE? */
  buffer_put_u16(&buffer, high_index);
  buffer_put_u16(&buffer, dragon_count_neighbor(&self->dragon));
  //printf("GEN-LOW-INDEX nodeid=%d low=%d\n", self->node_identifier, low_index);

  /* flow params */
  buffer_put_flow_param(&buffer, &self->flow_param);

  /* packet params */
#ifdef OLD_CODE  
  buffer_put_u16(&buffer, coded_packet->coef_pos_min);
  buffer_put_u16(&buffer, coded_packet->coef_pos_max);
  buffer_put_u16(&buffer, coded_packet->data_size);
  
  buffer_put_data(&buffer, coded_packet->content.u8,
		  sizeof(coded_packet->content.u8));
#else /* OLD_CODE */
  uint8_t* raw_symbol = NULL;
  unsigned int raw_size = dg_codec_pack_symbol(
      self->codec, coded_packet, &raw_symbol);
  if (raw_size > 0) {
    buffer_put_data(&buffer, raw_symbol, raw_size);
    free(raw_symbol);
  } else {
    assert(raw_symbol == NULL);
    buffer.has_bound_error = true;
  }
		     
#endif /* OLD_CODE */

  self->last_transmit_time = current_time;
  //dg_pywrite(stdout, self);
  if (!buffer.has_bound_error){
    return buffer.position;
  } else return 0;
}

/*--------------------------------------------------*/

#ifndef EMBEDDED

#define MAX_INTERVAL 1000000

broadcast_time_t dg_get_next_wakeup_time(broadcast_t* self, 
					 broadcast_time_t current_time)
{
  ASSERT( current_time >= self->current_time );
  self->current_time = current_time;

  if (self->is_nc_source) {
    self->pkt_interval = self->flow_param.pkt_interval;
    broadcast_time_t result = self->last_transmit_time + self->pkt_interval;
    return result;
  }

  int16_t dg_rank = dg_get_rank(self);
  double rate = dragon_compute_packet_rate(&self->dragon, dg_rank);

  double alpha = 1;
//~ #ifdef WITH_DYNAMIC_PKT_INTERVAL

  interval_t interval = MAX_INTERVAL;
  if (rate > 0) {
    interval = MAX_INTERVAL / rate ;
  } 

  interval_t exp_interval = alpha* interval + (1- alpha) * self->pkt_interval ;
  //~ uint16_t exp_interval = interval;
  self->pkt_interval = exp_interval;

  fprintf(stdout, " PacketRate %lf", rate);
  fprintf(stdout, "\nrate: n=%u d=%lf r=%lf rk=%d\n", (unsigned)self->node_identifier, (double)self->pkt_interval, (double)rate, (int)dg_rank);
  broadcast_time_t result = self->last_transmit_time + self->pkt_interval;
//~ #else /* WITH_DYNAMIC_PKT_INTERVAL */
  //~ fprintf(stdout, "PacketRate %d", rate);
  //~ broadcast_time_t interval = DEFAULT_PKT_INTERVAL;
  //~ broadcast_time_t result = self->last_transmit_time + interval; //
//~ #endif /* WITH_DYNAMIC_PKT_INTERVAL */

  if (current_time > result) 
    result = current_time;
  return result;
}
#endif /* EMBEDDED */

/*--------------------------------------------------*/

uint16_t dg_add_coded_packet(broadcast_t* self, dg_symbol_t* pkt)
{
  uint16_t packet_id = dg_codec_add_symbol(self->codec, pkt);
  return packet_id;
}

/*---------------------------------------------------------------------------*/

#ifdef WITH_PACKET_LIST
void packet_list_init(packet_list_t *list)
{ 
  list->count_pkt = 0;
  list->pos_pkt = 0;
}

bool_t packet_list_get_coded_packet (packet_list_t* list, dg_symbol_t* pkt)
{
  if (list->count_pkt == 0)
    return false;
  coded_packet_copy_from(pkt, &list->coded_packet[list->pos_pkt]);
  list->pos_pkt++;
  if (list->pos_pkt >= list->count_pkt)
    list->pos_pkt=0;
  return true;
}

#ifdef CONF_WITH_FPRINTF
void packet_list_pywrite(FILE* out, packet_list_t* list, broadcast_t* state)
{ 
  fprintf(out, "{ 'type':'packet-list'");
  fprintf(out, ", 'bufferSize':%u", list->count_pkt);  
  fprintf(out, ", 'transmittedPackets': %u", list->pos_pkt);
  fprintf(out, ", 'packetTable': {");
  uint16_t i;
  for (i=0; i<list->count_pkt; i++) {
    if (state->packet_set.id_to_pos[i] == COEF_POS_NONE) /* not in the packet set*/
      continue;
    dg_symbol_t* pkt =&list->coded_packet[i];
    if (pkt->coef_pos_min != COEF_POS_NONE) {
      fprintf(out, "%d:", i);
      coded_packet_pywrite(out,pkt);
    }
  }
  fprintf(out, " }");
  fprintf(out, " }");
}
#endif /* CONF_WITH_FPRINTF */

#endif /* WITH_PACKET_LIST */

/*---------------------------------------------------------------------------*/

#ifdef CONF_WITH_FPRINTF
void flow_param_pywrite(FILE* out, flow_param_t* param)
{ 
  fprintf(out, "{ 'type':'flowParam'");
  fprintf(out, ", 'pktInterval':%"PRI_DG_TIME, param->pkt_interval);
  fprintf(out, ", 'windowSize': %u", param->window_size);
  fprintf(out, ", 'nbPacket': %u", param->nb_packet);
  fprintf(out, " }");
}
#endif /* CONF_WITH_FPRINTF */

/*---------------------------------------------------------------------------*/

#ifdef CONF_WITH_FPRINTF
void dg_pywrite(FILE* out, broadcast_t* state)
{ 
  fprintf(out, "{ 'type':'dragonnet'");
  fprintf(out, ", 'nodeId':%u", state->node_identifier);
  fprintf(out, ", 'currentTime': %" PRI_DG_TIME, state->current_time);
  //fprintf(out, ", 'nextTime': %ld", state->next_time);
  fprintf(out, ", 'lastTransmitTable': %"PRI_DG_TIME,
	  state->last_transmit_time);
  fprintf(out, ", 'lastSeqNum': %u", state->last_seq_num);
  fprintf(out, ", 'isSource': %u", state->is_nc_source);
  fprintf(out, ", 'packetSet':");
#ifdef OLD_CODE  
  packet_set_pywrite(out, &state->packet_set);
#endif /* OLD_CODE */  
#warning "XXX: add back"
  //fprintf(out, ", 'packetList':");
  //packet_list_pywrite(out, &state->packet_list, state);
  fprintf(out, ", 'flowParam':");
  flow_param_pywrite(out, &state->flow_param);
  fprintf(out, ", 'dragon':");
  dragon_pywrite(out, &state->dragon, state->current_time);
  fprintf(out, " }");
}
#endif /* CONF_WITH_FPRINTF */

/*---------------------------------------------------------------------------*/

static bool_t get_decoded_packet
(dg_codec_t* packet_set, uint16_t required_min_coef_pos,
 dg_symbol_t* res_coded_packet)
{
#ifdef OLD_CODE  
  broadcast_t* self = (broadcast_t*) packet_set->notif_data;

  uint16_t i = required_min_coef_pos;
  uint16_t _L = packet_set->log2_nb_bit_coef;

  coded_packet_init(res_coded_packet, _L);

  uint8_t source_packet_content[CODED_PACKET_SIZE];
  uint16_t actual_size = 0;

#if 0
  if (self->callback == NULL 
      || self->callback->generate_source_packet_function == NULL) {
    actual_size = dg_generate_test_source_packet
      (self, i, source_packet_content, 6); /* XXX: packet size */
  } else {
    actual_size = self->callback->generate_source_packet_function
      (self, i, source_packet_content, CODED_PACKET_SIZE);
  }
#endif

#warning "XXX: should add callback for get_decoded_packet"
  actual_size = dg_generate_test_source_packet
    (self, i, source_packet_content, 6); /* XXX: packet size */

  if (actual_size == 0) 
    return false;

  coded_packet_init_from_base_packet(res_coded_packet, _L, i,
				     source_packet_content, actual_size);

#else /* OLD_CODE */
  WARN_NOT_REIMPLEMENTED;  
#endif /* OLD_CODE */

  
  return true;
}

/*---------------------------------------------------------------------------*/
