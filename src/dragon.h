/*---------------------------------------------------------------------------
 * DRAGON implementation
 *---------------------------------------------------------------------------
 * Author: Cedric Adjih
 * Copyright 2014 Inria
 * All rights reserved. Distributed only with permission.
 *---------------------------------------------------------------------------*/

#ifndef _DRAGON_H
#define _DRAGON_H

/*---------------------------------------------------------------------------*/

#include "general.h"
/*---------------------------------------------------------------------------*/

///#define WITH_DYNAMIC_PKT_INTERVAL
#define TIME_SCALE 1
//~ #define CONF_EXPIRE_INTERVAL 30// time in seconds
/*---------------------------------------------------------------------------*/

#ifdef CONF_DG_TIME_SEC
#define DG_TIME_SEC CONF_DG_TIME_SEC
#else /* CONF_DG_TIME_SEC */
#define DG_TIME_SEC 1000000 // time is in microsec
#endif /* CONF_DG_TIME_SEC */

//~ #ifdef CONF_EXPIRE_INTERVAL
//~ #define DG_EXPIRE_TIME (CONF_EXPIRE_INTERVAL*DG_TIME_UNIT)
//~ #else
//~ #define DG_EXPIRE_TIME (3*DG_TIME_UNIT) /* 3 UNIT */
//~ #endif

#define DG_TIME_UNIT (DG_TIME_SEC*TIME_SCALE)

#define DEFAULT_PKT_INTERVAL DG_TIME_UNIT
#define MIN_PKT_INTERVAL (DG_TIME_UNIT/8)
#define MAX_PKT_INTERVAL (DG_TIME_UNIT*8)

#define MAX_NEIGHBOR_NB_TWO_HOP 127u
#define MAX_NEIGHBOR_GAP 127u
#define GAP_SHIFT 8

/*---------------------------------------------------------------------------*/

#define DRAGON_MAX_NEIGHBOR 256

#define PRIORITY_NONE 0xffffu
#define NODE_ID_NONE  0xffffu
//~ uint16_t dg_expire_time;
#define RATE_ADAPTATION_CONSTANT 1

struct s_broadcast_config_t;
typedef struct s_dragon_info_t {
  uint16_t rank;
  uint16_t high_index;
  uint16_t low_index;
  uint16_t nb_neighbor;
  broadcast_time_t last_time;
} dragon_info_t;

typedef struct s_dragon_neighbor_t {
  uint16_t identifier;
  uint16_t priority;
  dragon_info_t info;
  uint16_t count_recv;
} dragon_neighbor_t;

//~ typedef struct s_dragon_config_t {
  //~ uint16_t conf_expire_interval;
//~ } dragon_config_t;

typedef struct s_dragon_t {
  dragon_neighbor_t neighbor[DRAGON_MAX_NEIGHBOR];
} dragon_t;

/*---------------------------------------------------------------------------*/

void dragon_init(dragon_t* self, struct s_broadcast_config_t* broadcast_config);

uint16_t dragon_count_neighbor(dragon_t* self);

void dragon_process(dragon_t* self, uint16_t neighbor_id,
		    dragon_info_t* neighbor_info);

uint16_t dragon_get_lowest_coef(dragon_t* self, broadcast_time_t current_time);

uint16_t dragon_get_lowest_rank(dragon_t* self, broadcast_time_t current_time);

double dragon_compute_packet_rate(dragon_t* self, uint16_t dg_rank);
/*---------------------------------------------------------------------------*/

#ifdef CONF_WITH_FPRINTF
void dragon_pywrite(FILE* out, dragon_t* self, broadcast_time_t current_time);
#endif /* WITH_FPRINTF */

/*---------------------------------------------------------------------------*/

#endif /* _DRAGON_H */
