/*---------------------------------------------------------------------------
 * DragonNet protocol
 *---------------------------------------------------------------------------
 * Author: Cedric Adjih, Hana Baccouch
 * Copyright 2013-2014 Inria
 * All rights reserved. Distributed only with permission.
 *---------------------------------------------------------------------------*/

#ifndef __DRAGONNET_H__
#define __DRAGONNET_H__

#ifdef __cplusplus
extern "C" {
#endif

/*---------------------------------------------------------------------------*/

#include <stdio.h>
  
#include "swif_full_symbol.h"

#include "dragonnet-config.h"
#include "general.h"
#include "dragon.h"
#include "dragonnet-codec.h"

/*---------------------------------------------------------------------------*/

/* Source symbol id none - XXX:TODO adjust as 32+ bits now */
#define COEF_POS_NONE 0xffffu  
#define PACKET_ID_NONE 0xffffu

#ifdef EMBEDDED
  typedef uint16_t interval_t;
#else /* EMBEDDED */
  typedef double interval_t;
#endif /* EMBEDDED */

typedef struct s_flow_param_t {
  interval_t pkt_interval;
  uint16_t window_size;
  uint16_t nb_packet;
} flow_param_t;
  
typedef struct s_broadcast_callback_t {
  void* callback_arg;

  void (*notify_packet_decoded_function)
  (void* arg, uint16_t packet_index);

  void (*notify_packet_set_full_function)
  (void* arg, uint16_t required_min_coef_pos);

  uint16_t (*generate_source_packet_function)
  (void* arg, uint16_t packet_id, uint8_t* packet_data,
   uint16_t max_packet_size);

} broadcast_callback_t;

typedef struct s_broadcast_config_t {
  uint16_t conf_expire_interval; // XXX: uint16_t
} broadcast_config_t;

typedef  struct s_broadcast_t {
  broadcast_time_t current_time;
  uint16_t         node_identifier;

  broadcast_time_t pkt_interval;
  broadcast_time_t last_transmit_time;
  uint16_t         last_seq_num;

  bool_t           is_nc_source;
  flow_param_t     flow_param; /** parameters of the coded flow */
  broadcast_config_t broadcast_config;
  //~ dragon_config_t dragon_config;
  dragon_t         dragon;

  broadcast_callback_t* callback;
  dg_codec_t*           codec;
} broadcast_t;

/*---------------------------------------------------------------------------*/

void dg_init(broadcast_t* self, uint16_t identifier, 
	     broadcast_time_t current_time,
	     flow_param_t* flow_param,
	     broadcast_config_t* broadcast_config,
	     broadcast_callback_t* callback,
	     dg_codec_t* codec);

uint16_t dg_process_coded_packet
(broadcast_t* self, uint8_t* packet_data, 
 uint16_t packet_size, broadcast_time_t current_time);

uint16_t dg_generate_coded_packet
(broadcast_t* self, uint8_t* packet_data, uint16_t max_packet_size, 
  broadcast_time_t current_time, dg_symbol_t* symbol);

/*XXX: rename */
dg_symbol_t* dg_create_coded_packet
(broadcast_t* self, uint16_t min_coef_pos, uint16_t max_coef_pos);

broadcast_time_t dg_get_next_wakeup_time(broadcast_t* self, 
  broadcast_time_t current_time);

#ifdef WITH_PACKET_LIST
bool_t packet_list_get_coded_packet (packet_list_t* list, coded_packet_t* pkt);
#endif /* WITH_PACKET_LIST */
  
uint16_t dg_add_coded_packet(broadcast_t* self, dg_symbol_t* pkt);

void dg_update_time(broadcast_t* self, broadcast_time_t current_time);

uint16_t dg_generate_test_source_packet
(void* arg, uint16_t packet_id, uint8_t* packet_data, uint16_t max_packet_size);

uint16_t dg_get_rank(broadcast_t* self);
void dg_update_rate(broadcast_t* self);
//To be Removed
#ifdef WITH_RECODE
bool_t packet_set_fill(packet_set_t* set, uint16_t packet_size); 
void packet_set_generate_coded_packet (packet_set_t* set, packet_list_t* list,
			       uint16_t coef_pos_min, uint16_t coef_pos_max,
				       coded_packet_t* pkt);
#endif /* WITH_RECODE */
//----

/*---------------------------------------------------------------------------*/
#ifdef CONF_WITH_FPRINTF
void dg_pywrite(FILE* out, broadcast_t* state);
//void packet_list_pywrite(FILE* out, packet_list_t* list, broadcast_t* state);
#endif /* CONF_WITH_FPRINTF */

#ifdef __cplusplus
}
#endif

#endif /* __DRAGONNET_H__ */
