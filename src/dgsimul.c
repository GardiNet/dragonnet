/*---------------------------------------------------------------------------
 *                      Simple Example of Simulator
 *  . Cedric Adjih, Hipercom Project-Team, INRIA Paris-Rocquencourt
 *  Copyright 2011 Institut National de Recherche en Informatique et
 *  en Automatique.  All rights reserved.  Distributed only with permission.
 *---------------------------------------------------------------------------*/

#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>

/*--------------------------------------------------*/

#include <assert.h>
#include "dragonnet-protocol.h"
#include "dragonnet-protocol.c"
/*--------------------------------------------------*/

#define MYFATAL(...) \
  do { fprintf(stderr, __VA_ARGS__); \
    exit(EXIT_FAILURE); } while(0)

#define MAX_NB_NODE 100
#define MAX_NB_LINK 8
 
broadcast_t* node_table[MAX_NB_NODE];
int link_table[MAX_NB_NODE][MAX_NB_LINK];
int nb_link_table[MAX_NB_NODE];

void simul_add_link(int from_id, int to_id)
{
  int last_case=0;
  int i;
  for (i=0; i<MAX_NB_LINK; i++) {
    if (link_table[from_id][i]==-1) {
      last_case=i;
      break;
    }
  }
  link_table[from_id][last_case]=to_id;
  printf("node table %d %d %d \n", from_id, to_id, last_case);
}

int main(int argc, char** argv)
{
  //~ //static simul_t simul;
  //~ /** -- init_node_table --*/
  //~ int i,j;
  //~ for (i=0; i<MAX_NB_NODE; i++) {
    //~ for (j=0; j<MAX_NB_LINK; j++) {
      //~ link_table[i][j]=-1;
    //~ }
  //~ }
  //~ 
  //~ /* get optional filename */ 
  //~ //hipsens_bool trace = HIPSENS_FALSE;
  //~ char* filename = NULL;
  //~ char* record_file_name = NULL;
  //~ (void) record_file_name;
  //~ int duration_msec = 3000;
  //~ int xsize = -1, ysize = -1;
  //~ if (argc>1) {
    //~ filename = argv[1];
    //~ argv ++;
    //~ argc --;
  //~ }
//~ 
  //~ while(argc > 1) {
    //~ /*if (!strcmp(argv[1], "trace")) {
      //~ trace = HIPSENS_TRUE;
      //~ argv ++;
      //~ argc --;
    //~ } else*/
    //~ if (!strcmp(argv[1], "duration")) {
      //~ duration_msec = atol(argv[2]);
      //~ argv += 2;
      //~ argc -= 2;
    //~ } else if (!strcmp(argv[1], "record")) {
      //~ record_file_name = argv[2];
      //~ argv += 2;
      //~ argc -= 2;
    //~ } else if (!strcmp(argv[1], "grid")) {
      //~ xsize = atoi(argv[2]);
      //~ argv += 2;
      //~ argc -= 2; 
    //~ } else break;
  //~ }
//~ 
  //~ /* read number of nodes */
  //~ int nb_node=4;
  //~ FILE* input = stdin;
   //~ 
  //~ if (xsize < 0) {
    //~ if (filename != NULL) {
      //~ input = fopen(filename, "r");
      //~ if (input == NULL) 
	//~ MYFATAL("%s: cannot open file '%s': %s\n", argv[0], filename, 
		//~ strerror(errno));
    //~ }
    //~ 
    //~ if (fscanf(input, "%d", &nb_node) != 1)
      //~ MYFATAL("%s: cannot read `nbNode'\n", argv[0]);
  //~ } else {
    //~ ysize = xsize;
    //~ nb_node = xsize*ysize;
    //~ ASSERT( nb_node <= MAX_NB_NODE );
  //~ }
//~ 
  //~ broadcast_time_t source_interval =150;
  //~ broadcast_time_t other_interval =50;
 //~ 
  //~ /*  if (fscanf(input, "%ld", &source_interval) != 1)
    //~ MYFATAL("%s: cannot read `source_interval'\n", argv[0]);
  //~ if (fscanf(input, "%ld", &other_interval) != 1)
    //~ MYFATAL("%s: cannot read `other_interval'\n", argv[0]);
  //~ */
//~ 
  //~ /* read links */
  //~ //simul_t simul_for_recording;
  //simul_init(&simul, nb_node);
  //simul.cfg_write_before_each_event = trace;
  //~ 
  //~ if (xsize < 0) {
    //~ for (;;) {
      //~ int from_id, to_id;
      //~ if (fscanf(input, "%d%d", &from_id, &to_id) != 2)
        //~ break;
      //~ simul_add_link(from_id, to_id);
      //~ simul_add_link(to_id, from_id);
    //~ }
  //~ } else {
  //~ /* -- this is a grid -- */
  //~ int x,y;
  //~ for (x=0;x<xsize;x++)
    //~ for (y=0;y<ysize;y++) {
      //~ int node_id = x + y*xsize;
      //~ int node_id_dx = (x+1) + y*xsize;
      //~ int node_id_dy = x + (y+1)*xsize;
      //~ if (x < xsize-1) {
        //~ simul_add_link(node_id, node_id_dx);
        //~ simul_add_link(node_id_dx, node_id);
      //~ }
      //~ if (y < ysize-1) {
        //~ simul_add_link(node_id, node_id_dy);
        //~ simul_add_link(node_id_dy, node_id);
      //~ }
    //~ }
  //~ }
   //~ 
  //~ /** -- nb_link_table -- **/
  //~ int nb_links=0; i=0;j=0;
  //~ for(i=0;i<nb_node;i++){
    //~ for(j=0;j<MAX_NB_LINK;j++){
      //~ if ((link_table[i][j])!=-1){
	//~ nb_links++;
      //~ }
      //~ else
	//~ break;
    //~ }
    //~ nb_link_table[i]=nb_links;
    //~ nb_links=0;
  //~ }
  //~ 
  //~ /* -- init nodes  -- */
  //~ bool_t is_source = 1; 
  //~ uint16_t source_node_id = 0; 
  //~ //init others 
  //~ int id;
  //~ for (id=0; id<nb_node; id++) {
    //~ broadcast_t* self = malloc(sizeof(*self));
    //~ if (id==source_node_id) {
      //~ dg_init(self, source_node_id, source_interval ,is_source);
    //~ }
    //~ else
    //~ dg_init(self, id, other_interval*i , !is_source);
  //~ 
    //~ node_table[id] = self;
    //~ //printf("node %d pkt_inteval %lu\n", self->node_identifier, self->pkt_interval);
  //~ }
  //~ /* run simulation */
  //~ /** Initialize source **/
  //~ broadcast_time_t current_time = 0; 
  //~ uint16_t nb_packet = 16;
  //~ broadcast_t* source_broadcast_t = node_table[source_node_id];
  //~ packet_set_t source_packet_set = source_broadcast_t->packet_set;
  //~ coded_packet_t* source_coded_packet = malloc(sizeof(*source_coded_packet)); 
  //~ uint8_t* packet_data = malloc(1000); 
  //~ //generate_source_packet (packet_id, source_coded_packet); 
  //~ uint16_t max_packet_size = 1000;
//~ 
  //~ /** packet_set_generate_coded_packet **/
  //~ packet_set_fill(&source_packet_set);
  //~ //packet_set_generate_coded_packet (&source_packet_set, source_coded_packet); 
  //~ //coded_packet_pywrite(stdout, &source_coded_packet);
  //~ 
  //~ /** dg_generate_coded_packet **/ 
  //~ /*uint16_t  packet_size = dg_generate_coded_packet (source_broadcast_t,
  //~ packet_data, max_packet_size, current_time, source_coded_packet);
  //~ printf("\npacket_size %d\n", packet_size);
  //~ */
//~ 
  //~ broadcast_time_t next_time=0;
  //~ while (current_time<duration_msec) {
	  //~ for (id=0; id<nb_node; id++) {
      //~ broadcast_t* self = node_table[id];
      //~ next_time = dg_get_next_wakeup_time(self, current_time);
      //~ self->next_time = next_time;
      //~ printf("next time %lu\n", next_time);
		//~ }
//~ 
    //~ /* Next time to generate_coded_source_packet */
    //~ int next_node = 0;
    //~ broadcast_time_t  min_next_time = node_table[0]->next_time;
  //~ 
    //~ for (i=1; i<nb_node; i++) {
      //~ if (((node_table[i]->next_time)<= min_next_time)& 
        //~ ((node_table[i]->next_time) > current_time)) {
        //~ min_next_time = node_table[i]->next_time;
        //~ next_node=i;
        //~ printf ("schedule next time id= %u next time= %lu \n", i, next_time);
        //~ (void)next_node;
      //~ }
     //~ }
    //~ current_time = min_next_time;
    //~ //uint16_t nb_packet=3;
    //~ broadcast_t* next_broadcast_t = node_table[next_node];
    //~ packet_set_t next_packet_set = next_broadcast_t->packet_set;
    //~ coded_packet_t next_coded_packet; 
    //~ //next_packet_set.coded_packet; 
   //~ 
    //~ printf("currrent time %lu  pkt interval %lu last transmit %lu\n",
     //~ current_time ,next_broadcast_t->pkt_interval,
    //~ next_broadcast_t->last_transmit_time);
    //~ next_broadcast_t->last_transmit_time= current_time;
   //~ 
   //~ 
    //~ /* -- generate_coded_source_packet -- */
    //~ ///	packet_set_fill(&next_packet_set, next_coded_packet);
    //~ packet_set_generate_coded_packet (&next_packet_set, &next_coded_packet);
    //~ 
    //~ /** dg_generate_coded_packet **/  
    //~ uint16_t  packet_size = dg_generate_coded_packet (next_broadcast_t,
    //~ packet_data, max_packet_size, current_time, &next_coded_packet);
  //~ 
    //~ /** dg_process_coded_packet **/ 
    //~ int source_nb_link= nb_link_table[source_node_id];
    //~ for (j=0; j<source_nb_link; j++) {
      //~ int link = link_table[source_node_id][j];
      //~ broadcast_t* receiver = node_table[link];
      //~ dg_process_coded_packet(receiver, packet_data, packet_size, current_time);
    //~ }
  //~ } 

  return 0;
}
/*---------------------------------------------------------------------------*/
