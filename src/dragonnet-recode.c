/*---------------------------------------------------------------------------
 * DragonNet protocol,
 * intermediate node recoding functions
 *---------------------------------------------------------------------------
 * Author: Cedric Adjih, Hana Baccouch
 * Copyright 2013 Inria
 * All rights reserved. Distributed only with permission.
 *---------------------------------------------------------------------------*/

#include "dragonnet.h"

/*---------------------------------------------------------------------------*/

/** Generate a random linear combination of packets inside the packet_set */
void packet_set_generate_coded_packet 
(packet_set_t* set, packet_list_t* list, 
 uint16_t coef_pos_min, uint16_t coef_pos_max,
 coded_packet_t* pkt)
{
  memset(pkt, 0, sizeof(coded_packet_t));
  coded_packet_init(pkt, set->log2_nb_bit_coef);
  coded_packet_t* tmp_coded_packet;

  uint16_t i;
  ///printf("RECODE: min=%d max=%d\n", coef_pos_min, coef_pos_max);
  for (i=0; i<MAX_CODED_PACKET; i++) {
    if (set->id_to_pos[i] != COEF_POS_NONE) {
#ifdef EMBEDDED
      uint8_t coef=(RAND_CLOCK^(uint8_t)rand()) % (1<<(1<<L));
      ///printf("COEF:%d\n", coef);
#else
      uint8_t coef=rand() % (1<<(1<<L));
#endif      
      tmp_coded_packet = &(set->coded_packet[i]);
      ASSERT( !coded_packet_was_empty(tmp_coded_packet) );
      if (tmp_coded_packet->coef_pos_min < coef_pos_min)
	continue;
      if (tmp_coded_packet->coef_pos_max > coef_pos_max)
	continue;
      coded_packet_add_mult (pkt, coef, tmp_coded_packet); //linear combination 
    }
  }

  ///printf("already decoded %08x:", (unsigned long int) set->get_decoded_packet_func);
  for (i=coef_pos_min;i<=coef_pos_max;i++) {
    if (bitmap_get_bit(set->decoded_bitmap, 
		       DECODED_BITMAP_SIZE, i)
	&& set->get_decoded_packet_func != NULL) {
      ///printf(" %d", i);
      coded_packet_t old_packet;
      if (set->get_decoded_packet_func(set, i, &old_packet)) {
#ifdef EMBEDDED
	uint8_t coef=(RAND_CLOCK^(uint8_t)rand()) % (1<<(1<<L));
	///printf("COEF:%d\n", coef);
#else
	uint8_t coef=rand() % (1<<(1<<L));
#endif      
	coded_packet_add_mult (pkt, coef, &old_packet); 
      }
    }
  } 
  ///printf("\n");
}

/*---------------------------------------------------------------------------*/

#if 0
/** Generate a random linear combination of packets inside the packet_set */
void packet_set_generate_coded_packet (packet_set_t* set, packet_list_t* list, 
				       coded_packet_t* pkt)
{
  memset(pkt, 0, sizeof(coded_packet_t));
  coded_packet_init(pkt, set->log2_nb_bit_coef);
  coded_packet_t* tmp_coded_packet;

  uint16_t i;
  for (i=0; i<MAX_CODED_PACKET; i++) {
    if (set->id_to_pos[i] != COEF_POS_NONE) {
#ifdef EMBEDDED
      uint8_t coef=(RAND_CLOCK^(uint8_t)rand()) % (1<<(1<<L));
#else
      uint8_t coef=rand() % (1<<(1<<L));
#endif
      tmp_coded_packet = &(set->coded_packet[i]);
      coded_packet_add_mult (pkt, coef, tmp_coded_packet); //linear combination 
    }
  }
}
#endif

/** Fill packet set with base packets P1, P2, ... Pk,  with data content 
    filled with bytes == base packet index and with size `packet_size' */
bool_t packet_set_fill(packet_set_t* set, uint16_t packet_size)
{ 
  uint16_t i;
  uint16_t window_size = 1<<(uint16_t)log2_window_size(set->log2_nb_bit_coef);
  if (window_size > MAX_CODED_PACKET)
    window_size = MAX_CODED_PACKET;

  for (i=0; i<window_size; i++) {    
    uint8_t packet[CODED_PACKET_SIZE];
    static reduction_stat_t stat;
    memset(packet, i, CODED_PACKET_SIZE);
    coded_packet_t coded_packet;
    memset(&coded_packet, 0, sizeof(coded_packet_t));
    coded_packet_init_from_base_packet(&coded_packet, L, i, 
				       packet, packet_size);
    uint16_t packet_id = packet_set_add(set, &coded_packet, &stat, false);
    ASSERT( packet_id != PACKET_ID_NONE );
  }
  packet_set_pywrite(stdout, set);
  return 0;
}

/*---------------------------------------------------------------------------*/
